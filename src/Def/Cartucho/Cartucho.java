/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Def.Cartucho;

/**
 *
 * @author JosepB
 */
public class Cartucho {
    //Definición de las 5 monedas que puede tractar con sus respectivas cantidades
    // para hacer un cartucho de cada tipo.
    private final int[][]cartucho =new int[][]{
        {20,25},
        {10,50},
        {5,50},
        {2,50},
        {1,50}};
    
    public int Moneda(int x){
        return cartucho[x][0];
    }
    
    public int Cuantidad(int x){
        return cartucho[x][1];
    }
}
