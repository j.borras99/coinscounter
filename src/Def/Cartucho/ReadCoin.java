/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Def.Cartucho;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author JosepB
 */
public class ReadCoin {
    private FileInputStream fr;
    private BufferedReader br;
    
    public ReadCoin(){
        try{
            fr= new FileInputStream("monedes.txt");
            br= new BufferedReader(new InputStreamReader(fr));
        }catch (FileNotFoundException fnfe){
            fnfe.getMessage();
        }
    }
    
    public String Read(){
        String s="";
        try{
            s=br.readLine();
        }catch(IOException ioe){
            ioe.getMessage();
        }return s;
    }
    
    public void Close(){
        try{
            fr.close();
        }catch(IOException ioe){
            ioe.getMessage();
        }
    }
}
