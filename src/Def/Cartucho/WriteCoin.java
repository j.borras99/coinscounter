/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Def.Cartucho;

import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author JosepB
 */
public class WriteCoin {
    
    private FileWriter fw = null;
    private int[] m;
    
    public WriteCoin(int[] mon){
        try{
        fw=new FileWriter("monedes.txt");
        this.m=mon;
        }catch(IOException ioe){
            ioe.getMessage();
        }
    }
    
    public void Write(){
        try{
            String res="";
            for(int i=0;i<m.length;i++){
                res+=m[i]+"\n";
            }
            fw.write(res);
        }catch(IOException ioe){
            ioe.getMessage();
        }
    }
    
    public void Close(){
        try{
            fw.close();
        }catch(IOException ioe){
            ioe.getMessage();
        }
    }
}
