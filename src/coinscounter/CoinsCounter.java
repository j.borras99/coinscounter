package coinscounter;

import Def.Cartucho.Cartucho;
import Def.Cartucho.ReadCoin;
import Def.Cartucho.WriteCoin;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Josep Borràs Sánchez
 */
public class CoinsCounter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        (new CoinsCounter()).Inici();
    }
    //Es el maximo de monedas diferentes que puede controlar el programa.
    //Si se añaden más monedas se debe canviar el valor de MAX
    private final int MAX = 5;
    private int[] qmon = new int[MAX];
            
    private void Inici(){
        boolean exit = false;
        int op;
        
        while (!exit){
            Mensaje("\n------------------------------------------\n"
                    + "              COINS COUNTER\n"
                    + "------------------------------------------\n"
                + "1.- Cartuchos\n"
                + "2.- Añadir monedas\n"
                + "3.- Retirar doblers\n"
                + "4.- Mostrar\n"
                + "5.- Total\n"
                + "0.- Salir \n\n"
                + "    Inserta una de les seguientes opciones:  ");
            
            op=ReadInt();
            
            if((op>=0)&&(op<=5)){
                switch(op){
                case 1:
                    String opc = "";
                    char ops;
                    
                    Mensaje("\n\n ¿Quieres añadir monedas (SI/NO)? ");
                    opc=ReadLine().toUpperCase();
                    switch(opc){
                        case "SI":
                            Anadir();
                            Count();
                            Mensaje("\n\n ¿Quieres hacer los cartuchos (SI/NO)? ");
                            ops = (ReadLine().toUpperCase()).charAt(0);
                            if(ops== 'S'){
                                Reset(qmon);
                                System.out.println("FET!");
                            }
                            break;
                            
                        case "NO":
                            Count();
                            Mensaje("\n\n ¿Quieres hacer los cartuchos (SI/NO)? ");
                            ops = (ReadLine().toUpperCase()).charAt(0);
                            if(ops== 'S'){
                                Reset(qmon); 
                                System.out.println("FET!");
                            }
                            break;
                    }
                    break;
                    
                case 2:
                    Anadir();
                    System.out.println("FET !\n\n");
                    break;
                    case 3:
                        Retirar();
                        break;    
                case 4:
                    String res="";
                    Cartucho c=new Cartucho();
                    for(int i=0;i<MAX;i++){
                        res+=(c.Moneda(i)+" centimos: "+(Lleguir())[i]+"\n");
                    }
                    Mensaje(res);
                    break;
                case 5:
                    String aux = "";
                    Cartucho ct = new Cartucho();
                    float total = 0;
                    float cantid;
                    for(int i=0; i<MAX; i++){
                        cantid = ct.Moneda(i)*(Lleguir())[i];
                        total += cantid;
                        aux += ct.Moneda(i)+" centimos: "+cantid/100+"€ \n";
                    }
                    aux += "--------------------------------\n"
                            + "TOTAL:    "+total/100+ " €";
                    Mensaje(aux);
                    break;
                case 0:
                    exit = true;
                    break;
                }
            }
        }
    }
    //Legueix les quantitas guardades en el fitxer i les va guardant dins una array.
    //Retorna la direccio de l'array on hi gurda els valor llegits. sempre ordenats
    //de 20centims fins a 1centim.
    private int[] Lleguir(){
        ReadCoin rc = new ReadCoin();
        String s;
        int []x=new int[MAX];
        int idx=0;
        
        s=rc.Read();
        while(s!=null){
            x[idx++]=Integer.parseInt(s);
            s=rc.Read();
        }
        rc.Close();
        return x;
    }
    //S'hagui elegit o no l'opcio d'afegir monedes, es gruardarà dins qmon les monesdes
    //que hi hagui en el fitxer.
    //Anam restatn la quantitat necessaria per fer cartuxos a la quantitat de monedes
    //que tenim fins que la quantitat de monedes es menor que la quantitat necessaria
    //per fer un cartuxo. Ens mostra les monedes que ens sobren i el nombre de cartuxos
    // que hem fet de cada tipus de moneda.
    private void Count(){
        Cartucho c = new Cartucho();
        qmon=Lleguir();
        
        for(int i=0;i<MAX;i++){
        int r;
        int ct=0;
        boolean acabat=true; 
            while(acabat){
                if(qmon[i]>=c.Cuantidad(i)){
                    r=qmon[i]-c.Cuantidad(i);
                    qmon[i]=r;
                    ct++;
                }else{
            acabat=false;
                }
            }
        Mensaje("\n*************************************\n"
                +c.Moneda(i)+" centimos: \n"
                + "cartuchos: "+ct+"\n"
                + "Monedas restatntes: "+qmon[i]);
        }
    }
    
    //Demana la quantitat de monedes que s'han d'afegir a les que ja teniem previament.
    //Lleguirà les monedes que ja teniem abans, les sumara i guardara el valor en el fitxer.
    private void Anadir(){
        Cartucho c = new Cartucho();
        int[] guardades=Lleguir();
        
        Mensaje("\n\nDi que cantidad de monedas tienes de cada tipo:\n");
        
        for(int i=0; i<MAX; i++){
            Mensaje(c.Moneda(i)+" centimos: ");
            qmon[i] = ReadInt()+guardades[i];
        }
        WriteCoin wc=new WriteCoin(qmon);
        wc.Write();
        wc.Close();
    }
    //Demana la quantitat de monedes que s'han d'afegir a les que ja tenim previament.
    //Legueix les monedes que ja tenim guardades, les restam i les tornam a gardar.
    private void Retirar(){
        Cartucho c = new Cartucho();
        int []guardades = Lleguir();
        
        Mensaje("\n\nDi quina quantitat de monesde vols retirar de cada tipus:\n");
        
        for(int i=0;i<MAX;i++){
            Mensaje(c.Moneda(i)+" centimos: ");
            qmon[i] = guardades[i]-ReadInt();
        }
        WriteCoin wc = new WriteCoin(qmon);
        wc.Write();
        wc.Close();
    }
    
    private void Reset(int[] x){
        WriteCoin wc=new WriteCoin(x);
        wc.Write();
        wc.Close();
    }
    
    private void Mensaje(String msg){
        System.out.print(msg);
        
    }
    
    private String ReadLine(){
        String aux=" ";
        try{
            BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
            aux = bfr.readLine();
        }catch(IOException ieo){
            ieo.getMessage();
        }
        return aux;
    }
    
    private int ReadInt(){
        int aux= 0;
        try{
            BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
            aux = Integer.parseInt(bfr.readLine());
        }catch(IOException ioe){
            ioe.getMessage();
        }
        return aux;
    }
}
